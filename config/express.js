'use strict';

/**
 * Module dependencies.
 */
var express = require('express'),
	morgan = require('morgan'),
	bodyParser = require('body-parser'),
	config = require('./config');

module.exports = function(db) {
	// Initialize express app
	var app = express();


  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(morgan('dev'));


	// Showing stack errors
	app.set('showStackError', true);

  // routing
  require('../app/routes/users-server-routes')(app);

  app.get('*', function(req,res){
    //res.sendFile(__dirname + '/public/views/index.html');
		res.sendFile('../public/views/index.html', { root: __dirname });
  });

  if (process.env.NODE_ENV === 'test') {
    config.port = 3000;
  }
  else{
    console.log("config.port, process.env.NODE_ENV: %s, %s", config.port, process.env.NODE_ENV);
  }



	return app;
};
