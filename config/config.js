module.exports = {
  "database": "mongodb://localhost/userstory-dev",
  "port": process.env.PORT || 3000,
  "secretKey": 'YourSecretKey',
  "app": {
		title: 'FancyApp - Development Environment'
	}
};
