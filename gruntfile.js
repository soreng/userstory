module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concurrent: {
			default: ['nodemon'],
			debug: ['nodemon', 'node-inspector'],
			options: {
				logConcurrentOutput: true,
				limit: 10
			}
		},
    env: {
			test: {
				NODE_ENV: 'test'
			},
			secure: {
				NODE_ENV: 'secure'
			}
		},
    nodemon: {
			dev: {
				script: 'server.js',
				options: {
					nodeArgs: ['--debug'],
					ext: 'js,html'
				}
			}
		}
  });

  require('load-grunt-tasks')(grunt);
  grunt.option('force', true);

  // Test task.
  //grunt.registerTask('test', ['env:test']);
  grunt.registerTask('default', ['env:test', 'concurrent:default']);

};
