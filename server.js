var express = require('express'),
	morgan = require('morgan'),
	bodyParser = require('body-parser');
var config = require('./config/config');
var mongoose = require('mongoose');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var db = mongoose.connect(config.database, function(err){
  if(err){
    console.log(err);
  }
  else{
    console.log("Connected to database");
  }
});

console.log("config.database: %s", config.database);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('dev'));

app.use(express.static(__dirname + '/public'));

var api = require('./app/routes/api')(app, express, io);
app.use('/api', api);


app.get('*', function(req,res){
  res.sendFile(__dirname + '/public/app/views/index.html');
  //res.sendFile('../public/views/index.html', { root: __dirname });
});

if (process.env.NODE_ENV === 'test') {
  config.port = 3000;
}
else{
  console.log("config.port, process.env.NODE_ENV: %s, %s", config.port, process.env.NODE_ENV);
}

var server = http.listen(config.port, function(err){
  if(err){
    console.log(err);
  }
  else{
    var host = server.address().address;
    var port = server.address().port;

    console.log("Listening at http://%s:%s", host, port);
  }
});

exports = module.exports = app;
