angular.module('MyApp', ['appRoutes', 'maincontroller', 'authService', 'usercontroller', 'userService', 'storyService', 'storycontroller'])
.config(function($httpProvider){
  $httpProvider.interceptors.push('AuthInterceptor');
});
